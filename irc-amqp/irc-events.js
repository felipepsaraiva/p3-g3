module.exports = function(proxy, amqp) {
  let irc = proxy.irc;

  irc.addListener('registered', function(message) {
    proxy.registered = true;
    amqp.sendIrcEvent(proxy.queue, 'registered');
  });

  irc.addListener('error', function (message) {
    amqp.sendIrcEvent(proxy.queue, 'irc error', {
      text: message.args[message.args.length - 1]
    });
  });

  irc.addListener('message#', function(nick, to, text, message) {
    amqp.sendIrcEvent(proxy.queue, 'message', {
      from: nick,
      to: to,
      text: text
    });
  });

  irc.addListener('pm', function(nick, text, message) {
    amqp.sendIrcEvent(proxy.queue, 'pm', {
      from: nick,
      text: text
    });
  });

  irc.addListener('join', function(channel, nick, message) {
    if (nick == proxy.nick)
      proxy.canais.push(channel);

    amqp.sendIrcEvent(proxy.queue, 'join', {
      channel: channel,
      nick: nick
    });
  });

  irc.addListener('part', function(channel, nick, reason, message) {
    if (nick == proxy.nick)
      proxy.canais.splice(proxy.canais.indexOf(channel), 1);

    amqp.sendIrcEvent(proxy.queue, 'part', {
      channel: channel,
      nick: nick,
      reason: reason
    });
  });

  irc.addListener('quit', function(nick, reason, channels, message) {
    amqp.sendIrcEvent(proxy.queue, 'quit', {
      nick: nick,
      reason: reason,
      channels: channels
    });
  });

  irc.addListener('whois', function(data) {
    amqp.sendIrcEvent(proxy.queue, 'whois', data);
  });

  irc.addListener('motd', function(data) {
    amqp.sendIrcEvent(proxy.queue, 'motd', data);
  });

  irc.addListener('topic', function(channel, topic, nick, message) {
    amqp.sendIrcEvent(proxy.queue, 'topic', {
      nick: nick,
      topic: topic,
      channel: channel
    });
  });

  irc.addListener('kick', function(channel, nick, by, reason, message) {
    amqp.sendIrcEvent(proxy.queue, 'kick', {
      channel: channel,
      nick: nick,
      reason: reason,
      by: by
    });
  });

  irc.addListener('nick', function(oldnick, newnick, channels, message) {
    amqp.sendIrcEvent(proxy.queue, 'nick', {
      oldnick: oldnick,
      newnick: newnick,
      channels: channels
    });
  });

  irc.addListener('names', function (channel, nicks) {
    amqp.sendIrcEvent(proxy.queue, 'names', {
      channel: channel,
      nicks: Object.keys(nicks),
      modifiers: nicks
    });
  });

  irc.addListener('kill', function (nick, reason, channels, message) {
    amqp.sendIrcEvent(proxy.queue, 'kill', {
      nick: nick,
      reason: reason,
      channels: channels
    });
  });

  irc.addListener('invite', function(channel, from, message){
    amqp.sendIrcEvent(proxy.queue, 'invite', {
      channel: channel,
      from: from
    });
  });

  irc.addListener('action', function (from, to, text, message) {
    amqp.sendIrcEvent(proxy.queue, 'action', {
      from: from,
      to: to,
      text: text
    });
  });

  irc.addListener('+mode', function (channel, by, mode, argument, message) {
    amqp.sendIrcEvent(proxy.queue, 'mode', {
      channel: channel,
      by: by,
      mode: '+' + mode,
      argument: argument
    });
  });

  irc.addListener('-mode', function (channel, by, mode, argument, message) {
    amqp.sendIrcEvent(proxy.queue, 'mode', {
      channel: channel,
      by: by,
      mode: '-' + mode,
      argument: argument
    });
  });

  irc.addListener('channellist_start', function (message) {
    amqp.sendIrcEvent(proxy.queue, 'channellist_start');
  });

  irc.addListener('channellist_item', function (channel) {
    amqp.sendIrcEvent(proxy.queue, 'channellist_item', channel);
  });

  irc.addListener('notice', function(nick, to, text, message) {
    if (!nick) nick = 'Server';

    amqp.sendIrcEvent(proxy.queue, 'notice', {
      from: nick,
      to: to,
      text: text
    });
  });
}
