const irc = require('irc');
const setupIrcListeners = require('./irc-events');

module.exports = function(proxies, amqp) {
  amqp.channel.assertQueue('registration', { durable: false });
  amqp.channel.consume('registration', function(msg) {
    let proxy, data = JSON.parse(msg.content.toString()), id = data.id;

    if (id) {
      if (proxies.hasOwnProperty(id))
        proxy = proxies[id];
      else
        return amqp.channel.sendToQueue(msg.properties.replyTo, Buffer.from( JSON.stringify({
          type: 'error',
          err: 'invalid id'
        }) ));
    } else {
      const crypto = require("crypto");
      do {
        id = crypto.randomBytes(16).toString("hex"); // Gera uma string aleatoria que será o id
      } while (proxies[id]); // Enquanto existir o id, tenta novamente

      proxy = {
        queue: 'user_' + id,
        nick: data.nome,
        servidor: data.servidor,
        manterAtivo: data.manterAtivo,
        canais: [],
        registered: false,

        irc: new irc.Client(data.servidor, data.nome, {
          userName: data.nome,
          realName: data.nome,
          channels: [data.canal]
        })
      };

      proxy.close = function(message) {
        amqp.channel.deleteQueue(this.queue);
        this.queue = 'trash';
        this.irc.disconnect(message);
        delete proxies[id];
      }

      proxies[id] = proxy;
      setupIrcListeners(proxy, amqp);
    }

    amqp.channel.sendToQueue(msg.properties.replyTo, Buffer.from( JSON.stringify({
      type: 'registration',
      data: {
        id: id,
        nick: proxy.nick,
        servidor: proxy.servidor,
        manterAtivo: proxy.manterAtivo,
        canais: proxy.canais,
        prefixCanal: proxy.irc.opt.channelPrefixes,
        registered: proxy.registered
      }
    }) ));
  }, { noAck: true });
}
