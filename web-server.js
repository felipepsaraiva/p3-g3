const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);

const path = require('path');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');

let amqp = {
	lib: require('amqplib/callback_api'),
	connection: null,
	channel: null,
	ready: false,
	send: function(queue, message, sendOptions) {
		this.channel.assertQueue(queue, { durable: false });

		if (typeof message !== 'string')
			message = JSON.stringify(message);

		this.channel.sendToQueue(queue, Buffer.from(message), sendOptions);
	}
};

amqp.lib.connect('amqp://localhost', function(err, conn) {
	if (err)
		return console.error(err);

	amqp.connection = conn;
  conn.createChannel(function(err, ch) {
		if (err)
			return console.error(err);

  	amqp.channel = ch;
		amqp.ready = true;
  });
});

let proxies = {};

app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static('public'));

app.get('/', function(req, res) {
  if (req.cookies.id)
    res.sendFile(path.join(__dirname, 'index.html'));
  else
		res.redirect('/login');
});

app.get('/login', function(req, res) {
	res.sendFile(path.join(__dirname, 'login.html'));
});

app.get('/login/id', function(req, res) {
	res.sendFile(path.join(__dirname, 'id.html'));
});

app.post('/login', function(req, res) {
	// Caso algum valor venha vazio, envia para a pagina de login novamente
	if (!req.body.id && !(req.body.servidor && req.body.nome && req.body.canal))
		return res.redirect('/');

	let id = req.body.id;
	if (id) {
		if (proxies[id]) { // Se o id já existe, volta para a tela de login
			res.clearCookie('id');
			return res.redirect('/');
		}

		proxies[id] = { id: id };
	} else {
		const crypto = require("crypto");
		do {
			id = 'temp_' + crypto.randomBytes(16).toString("hex"); // Gera uma string aleatoria que será o id
		} while (proxies[id]); // Enquanto existir o id, tenta novamente

		proxies[id] = {
			id: id,
			cadastro: {
				nome: req.body.nome,
				canal: req.body.canal,
				servidor: req.body.servidor,
				manterAtivo: !!req.body.manterAtivo // Força que o tipo seja boolean
			}
		};
	}

	res.cookie('id', id);
  res.redirect('/');
});

server.listen(process.env.PORT || 3000, function() {
  console.log('Server listening on port ' + (process.env.PORT || 3000) + '...');
});

const setupSocketListeners = require('./web-amqp/web-socket');
const setupAmqpListeners = require('./web-amqp/amqp');

io.on('connection', function(socket) {
	let id = socket.handshake.query.id;

	if (!proxies.hasOwnProperty(id))
		proxies[id] = { id: id };

	let proxy = proxies[id];
	proxy.socket = socket;
	proxy.amqp = amqp;

	// Modelo RPC (Tutorial 6 do RabbitMQ)
	// Cria uma fila de nome aleatório para fazer a comunicação
	// e envia o nome dessa fila para o outro proxy através do 'replyTo'
	amqp.channel.assertQueue('', { exclusive: true }, function(err, q) {
		if (err)
			return console.error(err);

		proxy.queue = q.queue;
		setupAmqpListeners(proxies, proxy);
		setupSocketListeners(proxy);

		if (proxy.cadastro)
			amqp.send('registration', proxy.cadastro, { replyTo: q.queue });
		else
			amqp.send('registration', { id: id }, { replyTo: q.queue });

		delete proxy.cadastro;
  });

	socket.on('error', function(error) {
		console.log('[SOCKET ERROR] ', error);
	});

	socket.on('disconnect', function() {
		if (!proxy.quit) {
			amqp.send('irc', {
				id: proxy.id,
				command: 'disconnect'
			});
		}

		if (proxy.consumerTag)
			amqp.channel.cancel(proxy.consumerTag);

		delete proxies[proxy.id];
	});
});
