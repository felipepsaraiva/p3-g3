// Tratamento dos eventos que o usuário pode fazer

function userEvents(socket) {
  // Evento de enviar um texto
  $('#mensagem-form').submit(function() {
    var mensagem = $('#mensagem').val().trim();
    $('#mensagem').val('');

    if (!mensagem)
      return false;

    if (mensagem.charAt(0) !== '/') {
      // Quando o usuario digita diretamente uma mensagem para o canal
      var canal = $('.canais-nav .active').attr('canal');
      if (!canal) {
        adicionarErro('Na aba inicial é necessário especificar o alvo através do comando /msg', $('.canais-nav .active').attr('canal') || 'inicio');
        return false;
      }

      var comando = '/msg ' + canal + ' ' + mensagem;
      comandos.msg(socket, comando.split(' '));
    } else {
      var args = mensagem.split(' ');
      var comando = args[0].substr(1);

      if (comandos.hasOwnProperty(comando))
        comandos[comando](socket, args);
      else
        adicionarErro(args[0] + ': Comando desconhecido!', $('.canais-nav .active').attr('canal') || 'inicio');
    }

    return false;
  });

  $('.canais-nav li').click(mudarTab);
  $('#atualizar-canais').click(function() {
    comandos.list(socket, ['/list']);
  });
  $('#atualizar-names').click(function() {
    var canal = $('.canais-nav .active').attr('canal');
    var comando = '/names ' + canal;
    comandos.names(socket, comando.split(' '));
    $('.name-ch-' + canal.replace('#', '')).remove();
  });

  // Clicar em uma das opções do menu de modos
  $('.mode-menu li').click(function() { return false; }); // Previne o fechamento do menu de modos
  $('.mode-menu span').click(function(evt) {
    var $target = $(this),
    mode = $target.closest('a').data('mode'),
    contexto = $('.canais-nav .active').attr('canal') || 'inicio',
    comando = '/mode ';

    if (mode.substr(0, 3) === 'ch-') {
      if (contexto === 'inicio')
        return adicionarErro('O modo selecionado é apenas para canais', contexto);
      else
        comando += contexto;
    } else {
      comando += globals.nick;
    }

    if ($target.hasClass('glyphicon-plus'))
      comando += ' +' + mode.replace('ch-', '');
    else
      comando += ' -' + mode.replace('ch-', '');

    comandos.mode(socket, comando.split(' '));
    return false;
  });

  $('#sair').click(function(){
    comandos.quit(socket, ['/quit']);
  });
} // Fim da função userEvents

var comandos = {};

comandos.nick = function(socket, args) {
  socket.emit('nick', args[1]);
}

// Envio de mensagens para canais e para usuários
comandos.msg = function(socket, args) {
  var data = {
    to: args[1],
    message: args.slice(2).join(' ')
  };

  socket.emit('message', data);
  var escopo = (isCanal(data.to) ? data.to : 'inicio');
  var alvo = (escopo == 'inicio' ? data.to : null);
  adicionarMensagem(data.message, escopo, globals.nick, alvo);
}

comandos.notice = function(socket, args) {
  if(args.length >= 2) {
    var data = {
      to: args[1],
      message: args.slice(2).join(' ')
    };

    socket.emit('notice', data);
  }
  // O notice não é adicionado no chat de quem enviou
}

comandos.names = function(socket, args) {
  if(args.length >= 2) {
    socket.emit('names', {
      channel: args[1]
    });
  }
}

comandos.list = function(socket, args) {
  socket.emit('list');
}

comandos.join = function(socket, args) {
  var canais = args.slice(1).join(',');
  socket.emit('join', canais);
}

comandos.motd = function(socket, args) {
  socket.emit('motd');
}

comandos.whois = function(socket, args) {
  if(args.length == 2)
    socket.emit('whois',args[1]);
}

comandos.mode = function(socket, args) {
  if (args.length >= 3)
    socket.emit('mode', args.slice(1));
}

comandos.part = function(socket, args) {
  if(args.length >= 2) {
    var data = {
      canal: args[1],
      mensagem: args.slice(2).join(' ')
    };

    socket.emit('part', data);
  }
}

comandos.quit = function(socket, args) {
  socket.emit('quit', args.slice(1).join(' '));
  Cookies.remove('id');
  window.location = "/";
}

comandos.action = function(socket, args) {
  if(args.length >= 3) {
    var data = {
      target: args[1],
      message: args.slice(2).join(' ')
    };

    socket.emit('action', data);
  }
}

comandos.id = function(socket, args) {
  var contexto = $('.canais-nav .active').attr('canal') || 'inicio';
  var id = Cookies.get('id');
  anunciar('O seu ID é: ' + id, contexto);
}
