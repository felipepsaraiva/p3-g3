var globals = {
	nick: null, // Nick do usuário (Inicializado no evento 'info inicial')
	servidor: null, // Servidor que o usuário conectou (Inicializado no evento 'info inicial')
	canais: [], // Canais que o usuário está (Inicializado no evento 'info inicial')
	prefixCanal: '#', // Caracteres que podem ser prefixo de canal
	registered: false // Indica se o proxy está conectado ao servidor irc fornecido
};

$(function() {
	var id = Cookies.get("id");
	var socket = io('/?id=' + id);

	initSocket(socket); // Função do initSocket.js
	userEvents(socket); // Função do userEvents.js

	globals.socket = socket;
	$('#mensagem').focus();
});

/*
	MURAL
	Funções auxiliares para adicionar texto ao mural
*/

// Adiciona um texto marcado no escopo fornecido
function anunciar(mensagem, escopo) {
	escopo = escopo.replace('#', 'ch-');
	adicionarTexto('<p class="anuncio text-center msg-' + escopo + '">' + mensagem + '</p>');
}

// Adiciona uma mensagem no escopo fornecido
function adicionarMensagem(mensagem, escopo, autor, alvo) {
	escopo = escopo.replace('#', 'ch-');
	var html = '<div class="row mensagem msg-' + escopo + '"><div class="col-xs-12 col-sm-3 col-md-2"><p>';

	var autorCru = autor.replace('[', '').replace(']', ''); // Analisa sem colchetes em caso de notice
	if (autorCru == 'Server' || autorCru == globals.nick)
		html += '<span class="text-info">' + autor + '</span>';
	else
		html += '<span class="autor">' + autor + '</span>';

	if (alvo) {
		html += ' => ';
		if (alvo == 'Server' || alvo == globals.nick)
			html += '<span class="text-info">' + alvo + '</span>';
		else
			html += '<span class="autor">' + alvo + '</span>';
	}

	html += '</p></div><div class="col-xs-12 col-sm-9 col-md-10">'
	  + '<p>' + mensagem + '</p>'
	  + '</div></div>';

	adicionarTexto(html);
}

function adicionarErro(mensagem, escopo) {
	escopo = escopo.replace('#', 'ch-');
	adicionarTexto('<p class="erro bg-danger text-danger text-center msg-' + escopo + '">' + mensagem + '</p>');
}

// Adiciona um texto no mural e ajusta o scroll do mural
function adicionarTexto(texto) {
  var mural = $("#mural");
  mural.append(texto);
	atualizarContexto();
	mural.scrollTop(mural[0].scrollHeight);
}

// Atualiza o mural para mostrar/esconder as mensagens que não pertencem ao escopo atual
function atualizarContexto() {
	var canal = $('.canais-nav .active').attr('canal');
	$('#mural').children().not('.msg-global').addClass('hidden');
	$('.painel-canais, .painel-names').addClass('hidden');
	$('.painel-names p[nome]').addClass('hidden');

	if (!canal) {
		$('.msg-inicio').removeClass('hidden');
		$('#titulo-painel').text('Canais');
		$('.painel-canais').removeClass('hidden');
		$('.mode-menu .ch-mode').addClass('hidden');
	} else {
		canal = canal.replace('#', '');
		$('.msg-ch-' + canal).removeClass('hidden');
		$('#titulo-painel').text('Nomes');
		$('.painel-names').removeClass('hidden');
		$('.mode-menu .ch-mode').removeClass('hidden');
		$('.name-ch-' + canal).removeClass('hidden');
	}
}

/*
	ABAS DE CANAIS
	Funções auxiliares para lidar com as abas dos canais
*/

// Adiciona uma nova aba de canal
function adicionarTab(canal, irParaTab) {
	var tab = $('<li canal="' + canal + '"><a>' + canal + '</a></li>');
	tab.click(mudarTab);
	$('.canais-nav').append(tab);
	if (irParaTab)
		mudarTab(null, tab);
}

// Remove uma aba de canal e exclui todas as mensagens que pertencem a ela
function removerTab(canal) {
	$('.msg-ch-' + canal.replace('#', '')).remove();
	$('.name-ch-' + canal.replace('#', '')).remove();
	var tab = $('.canais-nav li[canal="' + canal + '"]');

	if (tab.hasClass('active'))
		mudarTab(null, $('.canais-nav li').first());

	tab.remove();
}

// Função para tratar o evento de clicar em outra aba
function mudarTab(evt, tab) {
	if (!tab)
		tab = $(this);

	if (tab.hasClass('active'))
		return;

	$('.canais-nav li').removeClass('active');
	tab.addClass('active');
	atualizarContexto();
	$('#mural').scrollTop($('#mural')[0].scrollHeight);
}

/*
	PAINEIS LATERAIS
	Funções auxiliares para ações nos paineis laterais
*/

function adicionarCanal(canal, numUsuarios) {
	var item = $('<p>' + canal + ' (' + numUsuarios + ')</p>');
	item.click(onClickChannelItem);
	$('.painel-canais').append(item);
}

function onClickChannelItem() {
	var canal = $(this).text().split(' ')[0];
	$('#mensagem').val('/join ' + canal);
	$('#mensagem-form').submit();
}

function adicionarNome(canal, nome, modificador, atualizar) {
	canal = canal.replace('#', '');
	var item = $('<p nome="' + nome + '">' + modificador + nome + '</p>');
	item.addClass('name-ch-' + canal);

	if (nome === globals.nick)
		item.css({
			'text-decoration': 'underline',
			'cursor': 'default'
		});
	else
		item.click(onClickNameItem);

	$('.painel-names').append(item);
	if (atualizar)
		atualizarContexto();
}

function onClickNameItem() {
	$('#mensagem').val('/msg ' + $(this).attr('nome') + ' ');
	$('#mensagem').focus();
}

/*
	GENERICAS
	Funções auxiliares gerais
*/

function isCanal(str) {
	var c = globals.prefixCanal.split('');
	for (var i = 0 ; i < c.length ; i++) {
		if (str.charAt(0) === c[i])
			return true;
	}
	return false;
}
